import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ResponseDto} from '../interfaces/responseDto';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  constructor(private httpClient: HttpClient) {
  }

  getCursos() {
    // return this.httpClient.get<ResponseDto>(`http://localhost:8080/cursos`);
    // return this.httpClient.get<ResponseDto>(`https://cursos.ekeepoit.com/cursos`);
    return this.httpClient.get<ResponseDto>(`/assets/mocks/cursos.json`);
  }

  getCategorias() {
    return this.httpClient.get<ResponseDto>(`/assets/mocks/categorias.json`);
  }
}
