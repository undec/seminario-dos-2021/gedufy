import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {Curso} from '../../interfaces/curso';
import {ActionSheetController} from '@ionic/angular';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {SocialSharing} from '@ionic-native/social-sharing/ngx';
import {DataLocalService} from '../../services/data-local.service';

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  styleUrls: ['./curso.component.scss'],
})
export class CursoComponent implements OnInit, AfterViewInit {

  @Input() curso: Curso;
  @Input() nro: number;
  @Input() enFavoritos;

  constructor(private actionSheetController: ActionSheetController,
              private socialSharing: SocialSharing,
              private iab: InAppBrowser,
              private dataLocalService: DataLocalService) {
  }

  ngOnInit() {
    console.log(this.curso);
  }

  ngAfterViewInit(): void {
    console.log(this.curso);
  }

  abrirCurso() {
    const browser = this.iab.create(this.curso.url, '_system');
  }

  async lanzarMenu() {

    let borrarBtn;
    if (this.enFavoritos) {
      borrarBtn = {
        text: 'Borrar Favorito',
        icon: 'trash',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Borrar Favorito');
          this.dataLocalService.borrarCurso(this.curso);
        }
      };
    } else {
      borrarBtn = {
        text: 'Favorito',
        icon: 'star',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Favorito');
          this.dataLocalService.guardarCurso(this.curso);
        }
      };
    }

    const actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Compartir',
          icon: 'share',
          cssClass: 'action-dark',
          handler: () => {
            console.log('Share clicked');
            this.socialSharing.share(
              this.curso.nombre,
              null, // file
              this.curso.url
            );
          }
        },
        borrarBtn,
        {
          text: 'Cancelar',
          icon: 'close',
          cssClass: 'action-dark',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();
  }



}
