import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CursosComponent} from './cursos/cursos.component';
import {IonicModule} from '@ionic/angular';
import {CursoComponent} from './curso/curso.component';



@NgModule({
  declarations: [
    CursosComponent,
    CursoComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    CursosComponent,
    CursoComponent
  ]
})
export class ComponentsModule { }
